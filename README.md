Distributed database based on PgPool2 and PostrgreSQL 9.6
=========================================================

Example of usage middleware that works between PostreSQL servers and a
PostgreSQL database client. Both PgPoolII and two instances of
database servers (PostgreSQL 9.6) has been dockerized.  

### Ports
- PgPool2: 5432,
- Db1: 5441,
- Db2: 5442.

### Project based on: BetterVoice/pgpool2-container
(https://github.com/BetterVoice/pgpool2-container)

